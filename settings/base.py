"""Settings common to all instances of the project."""
import json
import os
from pathlib import Path
from os.path import abspath, dirname, join

from django.core.exceptions import ImproperlyConfigured

# JSON-based secrets module
with open('secrets.json') as ims_secret_key:
    secrets =  json.loads(ims_secret_key.read())


def get_secret(setting, secrets=secrets):
    """Get the secret variable or return explicit exception."""
    try:
        return secrets[setting]
    except KeyError:
        error_msg = 'Set the {0} environment variable!'.format(setting)
        raise ImproperlyConfigured(error_msg)


def root(*dirs):
    base_dir = join(dirname(__file__), '..', '..')
    return abspath(join(base_dir, *dirs))


# MEDIA CONFIGURATION BASED ON TWO SCOOPS 1.11
BASE_DIR = Path(__file__).resolve().parent.parent
MEDIA_ROOT = BASE_DIR / 'media'
STATIC_ROOT = BASE_DIR / 'static_root'
