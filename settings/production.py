"""This is the settings file used by your live production server(s). That is, the server(s) that host the real live website. This file contains production-level
settings only. It is sometimes called prod.py."""

import os

from .base import *

IMS_SECRET_KEY = os.environ['IMS_SECRET_KEY']
