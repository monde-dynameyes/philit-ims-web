import datetime

from django.db import models
from django.utils import timezone

from django.contrib.auth.models import User


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-
    updating ``created`` and ``modified`` fields.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Person(TimeStampedModel):
    """
    A class on customized/extension of information of the user
    """
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    birthday = models.DateTimeField()
    mobile_number = models.CharField(max_length=11)
