from django.urls import path

from . import views

app_name = 'registration'
urlpatterns = [
    path('home/', views.IndexView.as_view(), name="home"),
    path('registration/', views.RegistrationView.as_view(), name="register"),
    path('login/', views.LoginView.as_view(), name="login"),
    path('logout/', views.LogoutView.as_view(), name="logout"),
]
