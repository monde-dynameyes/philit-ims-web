import datetime
from django.shortcuts import render, redirect
from django.views.generic import View
from django.views.generic.edit import FormView
from django.urls import reverse_lazy

from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.messages.views import SuccessMessageMixin

# forms
from .forms import RegistrationForm

# other app
from officers.urls import *


class IndexView(View):
    template_name = 'registration/index.html'

    def get(self, request):
        return render(request, self.template_name)


class LoginView(AuthenticationForm, View):
    template_name = 'registration/login_user.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            user.last_login = datetime.datetime.now()
            user.save(update_fields=['last_login'])
            return redirect(reverse_lazy('registration:home'))
        else:
            messages.warning(request, 'Username or password are incorrect.')
        return render(request, self.template_name)


class LogoutView(View):

    def get(self, request):
        logout(request)
        messages.success(request, 'Your account has been logout successfully.')
        return redirect(reverse_lazy('registration:login'))


class RegistrationView(SuccessMessageMixin, FormView):
    template_name = 'registration/registration.html'
    form_class = RegistrationForm
    success_url = reverse_lazy('officers:members_list')
    sucess_message = "Sucessful registration, welcome to Phil-IT Jrs."

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Sucessful registration, welcome to Phil-IT Jrs.')
            return redirect(reverse_lazy('registration:register'))
        else:
            return render(request, 'registration/registration.html', {'form': form})
    else:
        form = RegistrationForm()

        args = {'form': form}
        return render(request, 'registration/registration.html', args)
