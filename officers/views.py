from django.shortcuts import render, redirect
from django.views.generic import View
from django.views.generic.edit import CreateView, DeleteView, UpdateView, FormView
from django.urls import reverse_lazy

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin

from . models import Officer
from . forms import OfficerForm

# Create your views here.


class OfficerView(View):
    template_name = 'officers/list_of_officers.html'

    def get(self, request):
        return render(request, self.template_name)


class CreateOfficer(SuccessMessageMixin, FormView):
    template_name = 'officers/create_officer.html'
    form_class = OfficerForm
    success_url = reverse_lazy('officers:officer')
    success_message = "Successfully added an officer to the list."

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class MemberView(SuccessMessageMixin, View):
    template_name = 'officers/members.html'

    def get(self, request):
        return render(request, 'officers/members.html')
