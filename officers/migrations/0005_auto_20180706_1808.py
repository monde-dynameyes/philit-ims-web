# Generated by Django 2.0.6 on 2018-07-06 10:08

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('officers', '0004_auto_20180704_1842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='officer',
            name='date_joined',
            field=models.DateField(default=datetime.datetime(2018, 7, 6, 18, 8, 41, 53421), verbose_name='Date joined'),
        ),
    ]
