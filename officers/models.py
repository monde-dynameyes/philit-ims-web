import datetime

from django.db import models
from django.utils import timezone
# Create your models here.


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-
    updating ``created`` and ``modified`` fields.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Officer(TimeStampedModel):

    POSITION = (
        ('pres', 'President'),
        ('vp', 'Vice President'),
        ('sec', 'Secretary'),
        ('tres', 'Treasurer'),
        ('au', 'Auditor'),
        ('pro', 'Peace Relation Officer')
    )
    full_name = models.CharField(max_length=200)
    position = models.CharField(max_length=100, choices=POSITION)
    date_joined = models.DateField("Date joined", default=datetime.datetime.now())
