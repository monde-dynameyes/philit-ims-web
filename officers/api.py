from .models import Officers


class JobResource(ModelResource):
    """
    API Facet
    """

    class Meta:
        queryset = Officers.objects.all()
