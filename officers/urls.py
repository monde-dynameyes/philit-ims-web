from django.urls import path

from . import views

app_name = 'officers'
urlpatterns = [
    path('officer_list', views.OfficerView.as_view(), name='officer'),
    path('add_officer', views.CreateOfficer.as_view(), name='create_officer'),
    path('members', views.MemberView.as_view(), name='members_list'),
]
