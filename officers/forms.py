from django import forms
from datetime import datetime
from django.contrib.auth.models import User

from .models import Officer
from .choices import POSITION


class OfficerForm(forms.ModelForm):
    full_name = forms.CharField(required=True, help_text='This field is required', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Full name',
        }
    ))

    position = forms.ChoiceField(required=True, help_text='Choose a position', choices=POSITION, widget=forms.Select(
        attrs={
            'class': 'form-control',
        }
    ))

    class Meta:
        model = Officer
        fields = (
            'full_name',
            'position',
        )

        def save(self, commit=True):
            officer = super(OfficerForm, self).save(commit=False)
            officer.full_name = self.cleaned_data['full_name']
            officer.position = self.cleaned_data['position']
            officer.date_joined = self.cleaned_data[datetime.datetime.now()]
            if commit:
                officer.save()
            return officer
