from django.contrib import admin
from . models import Officer
# Register your models here.


class OfficerProfile(admin.ModelAdmin):
    list_display = ('full_name', 'position', 'date_joined')


admin.site.register(Officer, OfficerProfile)
